import { AddMentorComponent } from './../add-mentor/add-mentor.component';
import { Mentor } from './../models/mentor';
import { MentorService } from './../services/mentor.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Component({
  selector: 'app-mentor',
  templateUrl: './mentor.component.html',
  styleUrls: ['./mentor.component.scss'],
})
export class MentorComponent implements OnInit {
  displayedColumns: string[] = [
    'name',
    'userType',
    'entity',
    'status',
    'action',
  ];
  dataSource = new MatTableDataSource();
  mentorData!: Mentor;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  name: any;
  animal: any;

  constructor(private mentorService: MentorService, public dialog: MatDialog) {
    this.mentorData = {} as Mentor;
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.getAllMentors();
  }

  getAllMentors() {
    this.mentorService.getList().subscribe((response: any) => {
      console.log(`response`, response);
      const newResponse = new Array();
      for (let index = 0; index < response.length; index++) {
        const element = response[index];
        newResponse.push({
          id: element._id,
          name: `${element.first_name} ${element.last_name}`,
          userType: element.company.user_type,
          entity: element.company.name,
          status: element.user_status,
        });
      }
      console.log(`newResponse`, newResponse);
      this.dataSource.data = newResponse;
    });
  }

  deleteData(id: string) {
    console.log(`id: `, id);
    this.mentorService.deleteId(id).subscribe((res: any) => {
      console.log(res);
      this.dataSource.data = this.dataSource.data.filter((el: any) => {
        return el.id !== id ? el : false;
      });
      console.log(`new this.dataSource.data: `, this.dataSource.data);
    });
  }

  showDialogAddMentor() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    this.dialog.open(AddMentorComponent, dialogConfig);
  }
}
