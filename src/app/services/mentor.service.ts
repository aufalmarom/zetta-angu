import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Mentor } from '../models/mentor';

@Injectable({
  providedIn: 'root',
})
export class MentorService {
  baseUrl = 'http://localhost:3000/mentor';

  constructor(private httpClient: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  // handleError(error: HttpErrorResponse) {
  //   if (error.error instanceof ErrorEvent) {
  //     console.error('error client or network: ', error.error.message);
  //   } else {
  //     console.error(
  //       `error code Back End: ${error.status}`,
  //       `error body: ${error.error}`
  //     );
  //   }

  //   return throwError('something went wrong');
  // }

  getList(): Observable<Mentor> {
    return this.httpClient.get<Mentor>(this.baseUrl);
  }

  postCreate(data: any): Observable<Mentor> {
    return this.httpClient.post<Mentor>(
      this.baseUrl,
      JSON.stringify(data),
      this.httpOptions
    );
  }

  getId(id: string): Observable<Mentor> {
    return this.httpClient.get<Mentor>(this.baseUrl + '/' + id);
  }

  putUpdate(id: string, item: any): Observable<Mentor> {
    return this.httpClient.put<Mentor>(
      this.baseUrl + '/' + id,
      JSON.stringify(item),
      this.httpOptions
    );
  }

  deleteId(id: string) {
    return this.httpClient.delete<Mentor>(
      this.baseUrl + '/' + id,
      this.httpOptions
    );
  }
}
