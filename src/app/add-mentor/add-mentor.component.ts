import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-mentor',
  templateUrl: './add-mentor.component.html',
  styleUrls: ['./add-mentor.component.scss'],
})
export class AddMentorComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<AddMentorComponent>) {}

  ngOnInit(): void {}

  onClose() {
    this.dialogRef.close();
  }

  onSubmit() {}

  onClear() {}
}
